# AdSD coding exercises: SQL (Structured Query Language)

Dit repository bevat een SQLite database met fictieve voorbeelddata (meer info over de dataset: http://2016.padjo.org/tutorials/sql-simplefolks-overview/) en een eenvoudige [Node.js](https://nodejs.org/en/) applicatie die verbinding maakt met de database. Tevens bevat ze een reeks unit tests voor elk van de oefeningen. Elke test case bevat een korte omschrijving (bv. 'Select all the data from the people'). De oefeningen bestaan eruit dat de (in de `query` constante in de test).

Open het `test.js` en vul de juiste query in bij elke unit test. Voer de unit tests uit met het `npm test` commando om te verifiëren of de correcte data opgehaald wordt.

Dependencies:
- [Node.js](https://nodejs.org/en/)

Meer informatie:
- [SQLite Simple Folks: Overview ](http://2016.padjo.org/tutorials/sql-simplefolks-overview/)
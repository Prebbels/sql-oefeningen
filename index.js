const sqlite3 = require('sqlite3').verbose();

const closeDatabase = (db, callback) => {
  db.close((err) => {
    if (err) {
      console.error(err.message);
    }
    callback();
  });
}

function performQuery(query, callback) {

  // connect to database
  let db = new sqlite3.Database('./simplefolks.sqlite', (err) => {
    if (err) {
      console.error(err);
    }
  });

  // perform query
  var rows = [];
  db.serialize(() => {
    db.each(query, (err, row) => {
      if (err) {
        console.error(err.message);
      }
      rows.push(row);
    });

    // close the database and pass the data to the test
    closeDatabase(db, () => {
      callback(rows);
    });
  });
}

module.exports = performQuery;
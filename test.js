const performQuery = require("./index.js");

//
//  SELECT and FROM
//

test("Select all the data from the people", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { name: "Austin", sex: "M", age: 33 },
        { name: "Blair", sex: "M", age: 90 },
        { name: "Carolina", sex: "F", age: 28 },
        { name: "Dani", sex: "F", age: 41 },
        { name: "Donald", sex: "M", age: 70 },
        { name: "Eliza", sex: "F", age: 37 },
        { name: "Farida", sex: "F", age: 23 },
        { name: "Georgina", sex: "F", age: 19 },
        { name: "Hillary", sex: "F", age: 68 },
        { name: "Leland", sex: "M", age: 16 },
        { name: "Liam", sex: "M", age: 22 },
        { name: "Michael", sex: "M", age: 48 },
        { name: "Phoebe", sex: "F", age: 52 },
        { name: "Sherry", sex: "F", age: 39 },
        { name: "Zed", sex: "M", age: 42 },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("Select just pet name and owner_name from pets", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { name: "Maru", owner_name: "Austin" },
        { name: "Icey", owner_name: "Blair" },
        { name: "Maxie", owner_name: "Blair" },
        { name: "Rex", owner_name: "Carolina" },
        { name: "Harambe", owner_name: "Dani" },
        { name: "Syd", owner_name: "Dani" },
        { name: "Artemis", owner_name: "Dani" },
        { name: "Mr. Muggles", owner_name: "Donald" },
        { name: "Meowser", owner_name: "Donald" },
        { name: "Donald", owner_name: "Donald" },
        { name: "Hodor", owner_name: "Eliza" },
        { name: "Bumpkin", owner_name: "Georgina" },
        { name: "Secretariat", owner_name: "Hillary" },
        { name: "Socks", owner_name: "Hillary" },
        { name: "Sir Barks-Alot", owner_name: "Liam" },
        { name: "Harry Potter", owner_name: "Leland" },
        { name: "Xerses", owner_name: "Michael" },
        { name: "Zeus", owner_name: "Michael" },
        { name: "Rocket", owner_name: "Phoebe" },
        { name: "Kermit", owner_name: "Sherry" },
        { name: "Hector", owner_name: "Sherry" },
        { name: "Essy", owner_name: "Sherry" },
        { name: "Samwise", owner_name: "Sherry" },
        { name: "Jenkins", owner_name: "Zed" },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

//
//  ORDER and LIMIT
//

test("List the oldest person in people", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([{ name: "Blair", sex: "M", age: 90 }]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("List the 3 most expensive homes in homes", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { owner_name: "Donald", area: "urban", value: 660000 },
        { owner_name: "Donald", area: "urban", value: 450000 },
        { owner_name: "Hillary", area: "urban", value: 400000 },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

//
//  WHERE and LIKE
//

test("List all the dogs in alphabetical order of their owners' names", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { name: "Icey", owner_name: "Blair", type: "dog" },
        { name: "Maxie", owner_name: "Blair", type: "dog" },
        { name: "Rex", owner_name: "Carolina", type: "dog" },
        { name: "Syd", owner_name: "Dani", type: "dog" },
        { name: "Socks", owner_name: "Hillary", type: "dog" },
        { name: "Sir Barks-Alot", owner_name: "Liam", type: "dog" },
        { name: "Kermit", owner_name: "Sherry", type: "dog" },
        { name: "Hector", owner_name: "Sherry", type: "dog" },
        { name: "Essy", owner_name: "Sherry", type: "dog" },
        { name: "Samwise", owner_name: "Sherry", type: "dog" },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("List the 3 most expensive homes in the country", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { owner_name: "Hillary", area: "country", value: 380000 },
        { owner_name: "Zed", area: "country", value: 177000 },
        { owner_name: "Georgina", area: "country", value: 82000 },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("List all pets whose names start with a P or end with an E", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { name: "Maxie", type: "dog", owner_name: "Blair" },
        { name: "Harambe", type: "bird", owner_name: "Dani" },
        { name: "Samwise", type: "dog", owner_name: "Sherry" },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

//
//  More practice with conditional logic
//

test("List all people in alphabetical order of name, who are older than 30 but younger than 60", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { name: "Austin", sex: "M", age: 33 },
        { name: "Dani", sex: "F", age: 41 },
        { name: "Eliza", sex: "F", age: 37 },
        { name: "Michael", sex: "M", age: 48 },
        { name: "Phoebe", sex: "F", age: 52 },
        { name: "Sherry", sex: "F", age: 39 },
        { name: "Zed", sex: "M", age: 42 },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("List all people whose names start with a vowel", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { name: "Austin", sex: "M", age: 33 },
        { name: "Eliza", sex: "F", age: 37 },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

//
//  GROUP BY and Aggregate functions
//

test("List the count of males and females in people", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        {
          "COUNT(*)": 8,
          sex: "F",
        },
        {
          "COUNT(*)": 7,
          sex: "M",
        },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("Find the average home value in homes", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        {
          average_value: 212350,
        },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("Who owns the highest cumulative value of homes?", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        {
          "SUM(value)": 1370000,
          owner_name: "Donald",
        },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

//
//  JOIN tables
//

test("Show every home's value along with their owner's name and age", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { age: 33, name: "Austin", value: 145000 },
        { age: 90, name: "Blair", value: 95000 },
        { age: 28, name: "Carolina", value: 220000 },
        { age: 28, name: "Carolina", value: 190000 },
        { age: 41, name: "Dani", value: 67000 },
        { age: 70, name: "Donald", value: 450000 },
        { age: 70, name: "Donald", value: 260000 },
        { age: 70, name: "Donald", value: 660000 },
        { age: 37, name: "Eliza", value: 210000 },
        { age: 23, name: "Farida", value: 180000 },
        { age: 19, name: "Georgina", value: 82000 },
        { age: 68, name: "Hillary", value: 380000 },
        { age: 68, name: "Hillary", value: 400000 },
        { age: 22, name: "Liam", value: 160000 },
        { age: 16, name: "Leland", value: 42000 },
        { age: 48, name: "Michael", value: 160000 },
        { age: 48, name: "Michael", value: 82000 },
        { age: 52, name: "Phoebe", value: 77000 },
        { age: 39, name: "Sherry", value: 210000 },
        { age: 42, name: "Zed", value: 177000 },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});

test("Show the name and sex of every dog owner over 40", (done) => {
  const callback = (data) => {
    try {
      expect(data).toEqual([
        { name: "Blair", sex: "M" },
        { name: "Blair", sex: "M" },
        { name: "Dani", sex: "F" },
        { name: "Hillary", sex: "F" },
      ]);
      done();
    } catch (error) {
      done(error);
    }
  };

  // insert the correct query in the constant below
  const query = "";
  performQuery(query, callback);
});
